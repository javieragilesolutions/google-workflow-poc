class Step {
  constructor(step) {
    this.step = step;
  }

  get() {
    return this.step;
  }

  set(step) {
    this.step = step;
  }

}

module.exports = Step;