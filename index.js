const express = require('express')
const { WorkflowsClient, ExecutionsClient } = require('@google-cloud/workflows');
const step = require('./Step');
const app = express()
const port = 3000

const projectId = 'schema-builder-307022';
const location = 'us-central1';
const name = 'POC-Automated-Task-SDK';
const excecutionId = 'e245b831-06c5-4ae8-b6b7-8c5704eca1e7'

const wfClient = new WorkflowsClient();
const ExClient = new ExecutionsClient();

app.get('/workflows', async (req, res) => {
  const [workflows] = await wfClient.listWorkflows({
    parent: wfClient.locationPath(projectId, location)
  });
  for (const workflow of workflows) {
    console.info(`name: ${workflow.name}`);
  }
  res.sendStatus(200)
})

app.get('/excecute', async (req, res) => {
  const [resp] = await ExClient.createExecution({
    parent: ExClient.workflowPath(projectId, location, name)
  });
  console.info(`name: ${resp.name}`);
  res.sendStatus(200)
})

app.get('/excecution', async (req, res) => {
  const excecution = await ExClient.getExecution({
    name: ExClient.executionPath(projectId, location, name, excecutionId)
  });
  console.log(excecution[0].result)
  res.sendStatus(200)
})

app.get('/create', async (req, res) => {
  const stepContent = "- firstStep:\n    return: Hello World from NodeJs!";
  const wfStep = new step(stepContent);
  
  try {
    const [workflow] = await wfClient.createWorkflow({
      parent: wfClient.locationPath(projectId, location),
      workflow: {
        name: "AutomatedWorkFlow",
        description: "WorkFlow created from NodeJs",
        sourceContents: wfStep.get(),
      },
      workflowId: "POC-Automated-Task-SDK"
    });
    res.status(200).send(workflow.name);
  } catch (error) {
    console.error(error);
    const buffer = error.metadata.internalRepr.get('google.rpc.badrequest-bin')[0];
    console.log(buffer.toString())
  }
});



app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})